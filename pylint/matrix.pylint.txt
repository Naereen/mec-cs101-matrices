************* Module matrix
W:142, 0: FIXME We should forbid modifying these attributes from outside the class (fixme)
W:936, 0: XXX This is coefficient wise ! (fixme)
W:1023, 0: XXX should never happen! (fixme)
W:1175, 0: XXX Do we really have a singular matrix already ? (fixme)
W:1179, 0: XXX not yet ! (fixme)
W:1187, 0: XXX Shouldn't we swap rows instead? I think not (fixme)
W:1219, 0: FIXME isn't it a cause of rounding mistake? (fixme)
W:1763, 0: TODO: Try to find an algorithm to approximatively compute eigen values, and eigen vectors ? (fixme)
W:2356, 0: WARNING Accessing array[i] does not check if i is a good index or not (fixme)
W:2413, 0: XXX Typo in the subject (fixme)
W:2562, 0: TODO Solver for linear equation A.x = b (fixme)
C:  1, 0: Too many lines in module (2580/1000) (too-many-lines)
W:259,27: Catching too general exception Exception (broad-except)
W:262,31: Catching too general exception Exception (broad-except)
W:276,27: Catching too general exception Exception (broad-except)
W:279,31: Catching too general exception Exception (broad-except)
W:293,31: Catching too general exception Exception (broad-except)
W:296,35: Catching too general exception Exception (broad-except)
R:222, 4: Too many branches (20/12) (too-many-branches)
W:439,15: Catching too general exception Exception (broad-except)
W:475,15: Catching too general exception Exception (broad-except)
W:503,15: Catching too general exception Exception (broad-except)
W:532,15: Catching too general exception Exception (broad-except)
W:1098,20: Redefining name 'det' from outer scope (line 2139) (redefined-outer-name)
R:1098, 4: Too many local variables (16/15) (too-many-locals)
W:1143,19: Catching too general exception Exception (broad-except)
W:1150,19: Catching too general exception Exception (broad-except)
R:1098, 4: Too many branches (34/12) (too-many-branches)
R:1098, 4: Too many statements (76/50) (too-many-statements)
W:1238,27: Redefining name 'inv' from outer scope (line 2173) (redefined-outer-name)
R:1238, 4: Too many local variables (18/15) (too-many-locals)
W:1256,19: Catching too general exception Exception (broad-except)
W:1262,19: Catching too general exception Exception (broad-except)
R:1238, 4: Too many branches (48/12) (too-many-branches)
R:1238, 4: Too many statements (97/50) (too-many-statements)
E:1716,12: function already defined line 1681 (function-redefined)
C:1716,12: Missing function docstring (missing-docstring)
C:1836, 8: Missing function docstring (missing-docstring)
R:111, 0: Too many public methods (42/20) (too-many-public-methods)
W:2139, 8: Redefining name 'A' from outer scope (line 2578) (redefined-outer-name)
W:2152, 9: Redefining name 'A' from outer scope (line 2578) (redefined-outer-name)
W:2161,10: Redefining name 'A' from outer scope (line 2578) (redefined-outer-name)
W:2167,17: Redefining name 'A' from outer scope (line 2578) (redefined-outer-name)
W:2173, 8: Redefining name 'A' from outer scope (line 2578) (redefined-outer-name)
W:2182, 8: Redefining name 'A' from outer scope (line 2578) (redefined-outer-name)
W:2201,21: Redefining name 'A' from outer scope (line 2578) (redefined-outer-name)
W:2227,15: Catching too general exception Exception (broad-except)
W:2234,15: Catching too general exception Exception (broad-except)
R:2201, 0: Too many branches (21/12) (too-many-branches)
W:2298, 9: Redefining name 'A' from outer scope (line 2578) (redefined-outer-name)
W:2303,10: Redefining name 'A' from outer scope (line 2578) (redefined-outer-name)
W:2535,10: Redefining name 'A' from outer scope (line 2578) (redefined-outer-name)
W:2543,13: Redefining name 'A' from outer scope (line 2578) (redefined-outer-name)
W:2551,13: Redefining name 'A' from outer scope (line 2578) (redefined-outer-name)


Report
======
686 statements analysed.

Statistics by type
------------------

+---------+-------+-----------+-----------+------------+---------+
|type     |number |old number |difference |%documented |%badname |
+=========+=======+===========+===========+============+=========+
|module   |1      |0          |+1.00      |100.00      |0.00     |
+---------+-------+-----------+-----------+------------+---------+
|class    |3      |3          |=          |100.00      |0.00     |
+---------+-------+-----------+-----------+------------+---------+
|method   |70     |0          |+70.00     |100.00      |0.00     |
+---------+-------+-----------+-----------+------------+---------+
|function |31     |0          |+31.00     |93.55       |6.45     |
+---------+-------+-----------+-----------+------------+---------+



Raw metrics
-----------

+----------+-------+------+---------+-----------+
|type      |number |%     |previous |difference |
+==========+=======+======+=========+===========+
|code      |744    |29.13 |744      |=          |
+----------+-------+------+---------+-----------+
|docstring |1357   |53.13 |1357     |=          |
+----------+-------+------+---------+-----------+
|comment   |124    |4.86  |124      |=          |
+----------+-------+------+---------+-----------+
|empty     |329    |12.88 |329      |=          |
+----------+-------+------+---------+-----------+



Duplication
-----------

+-------------------------+------+---------+-----------+
|                         |now   |previous |difference |
+=========================+======+=========+===========+
|nb duplicated lines      |0     |0        |=          |
+-------------------------+------+---------+-----------+
|percent duplicated lines |0.000 |0.000    |=          |
+-------------------------+------+---------+-----------+



Messages by category
--------------------

+-----------+-------+---------+-----------+
|type       |number |previous |difference |
+===========+=======+=========+===========+
|convention |3      |0        |+3.00      |
+-----------+-------+---------+-----------+
|refactor   |9      |0        |+9.00      |
+-----------+-------+---------+-----------+
|warning    |41     |3        |+38.00     |
+-----------+-------+---------+-----------+
|error      |1      |0        |+1.00      |
+-----------+-------+---------+-----------+



Messages
--------

+------------------------+------------+
|message id              |occurrences |
+========================+============+
|broad-except            |16          |
+------------------------+------------+
|redefined-outer-name    |14          |
+------------------------+------------+
|fixme                   |11          |
+------------------------+------------+
|too-many-branches       |4           |
+------------------------+------------+
|too-many-statements     |2           |
+------------------------+------------+
|too-many-locals         |2           |
+------------------------+------------+
|missing-docstring       |2           |
+------------------------+------------+
|too-many-public-methods |1           |
+------------------------+------------+
|too-many-lines          |1           |
+------------------------+------------+
|function-redefined      |1           |
+------------------------+------------+



Global evaluation
-----------------
Your code has been rated at 9.15/10 (previous run: 9.96/10, -0.80)
So close to being perfect...

